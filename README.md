schedler.co
===========

website and contents of schedler.co / schedler.pro

Last update 22.02.2017

Changelog
=========

22.02.2017 

* Version 2.0 
* Relaunch with webslides

05.07.2016 

* PURR is open for business
* PURR sections added to CV

30.04.2016 

* first teaser for PURR
* some minor updates 

18.11.2014 

* Minor improvements for code quality.
* using `<em>` instead of `<i>`
* using `<strong>` instead of `<b>`
* removed broken `<i>` tags

15.06.2014

* Minor updates
* Migration from kanso to Grunt

23.04.2014

* added local assets (not in git)

14.04.2014

* removed forth station 
* add mobile again
* add links again
* add button again 